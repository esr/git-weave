# Project makefile for git-weave
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed <git-weave -n -e '/^version *= *"\(.*\)"/s//\1/p')

SOURCES = README COPYING NEWS git-weave git-weave.txt git-weave.1 Makefile \
	control git-weave-logo.png

all: git-weave.1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .txt .1

.txt.1:
	asciidoctor -D. -a nofooter -b manpage $<
.txt.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

install: git-weave.1
	install -d "$(target)/bin"
	install -d "$(target)/$(mandir)/man1"
	install -m 755 git-weave "$(target)/bin"
	install -m 644 git-weave.1 "$(target)/$(mandir)/man1"

uninstall:
	rm "$(target)/$(mandir)/man1/git-weave.1"
	rm "$(target)/bin/git-weave"
	-rmdir -p "$(target)/$(mandir)/man1"
	-rmdir -p "$(target)/bin"

pylint:
	@pylint --score=n git-weave

clean:
	rm -fr *.html *.1 MANIFEST

check:
	cd tests; $(MAKE) --quiet

reflow:
	@black git-weave

version:
	@echo $(VERS)

git-weave-$(VERS).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:git-weave-$(VERS)/: >MANIFEST
	@(cd ..; ln -s git-weave git-weave-$(VERS))
	(cd ..; tar -czf git-weave/git-weave-$(VERS).tar.gz `cat git-weave/MANIFEST`)
	@ls -l git-weave-$(VERS).tar.gz
	@(cd ..; rm git-weave-$(VERS))

dist: git-weave-$(VERS).tar.gz

NEWSVERSION=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

release: git-weave-$(VERS).tar.gz git-weave.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper version=$(VERS) | sh -e -x

refresh: git-weave.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -e -x
